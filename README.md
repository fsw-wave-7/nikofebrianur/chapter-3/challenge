Link Postman
https://www.getpostman.com/collections/9d2b051231e9f1282e9e


--- 
npm init -y
npm install pg

---
npm install sequelize
sequelize init

--- 
edit config.json sesuai database

--- bikin model --name dan --attributes (jadi kolom)
sequelize model:generate --name User --attributes name:string,username:string,age:integer,password:string 
sequelize db:migrate 

--- add di package.json
sequelize db:migrate 

--- bikin seeder
sequelize seed:create --name (isi nama file) 
sequelize db:seed:all
sequelize db:seed:undo --seed (name-of-seed-as-in-data) 