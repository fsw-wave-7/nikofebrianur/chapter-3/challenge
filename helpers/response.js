/// declare messages
const pesan = { 
    "200": "Success",
    "201": "Data saved"
} 

/// function response
function response(res,code,data,meta={})
    {res.status(code).json({
        data:data,
        meta: {
            code:code,
            message: pesan[code.toString()],
            ...meta
        }
    })
}

module.exports = {response} /// export module