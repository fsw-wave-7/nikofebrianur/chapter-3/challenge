const { Router } = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const AuthMiddleware = require('../middlewares/AuthMiddleware')

/// export controller
const AuthController = require('../controllers/web/AuthController')
const HomeController = require('../controllers/web/HomeController')

const web = Router()

/// parser
web.use(bodyParser.json())
web.use(bodyParser.urlencoded({extended:true}))
web.use(cookieParser())

const authController = new AuthController
const homeController = new HomeController

web.get('/', homeController.index)

/// login
web.get('/login', authController.login)
web.get('/login', authController.doLogin)
web.get('/logout', authController.logout)

web.use(AuthMiddleware())

/// user 
web.get('/add', homeController.addUser)
web.post('/save-user', homeController.saveUser)

web.get('/edit/:id', homeController.editUser)
web.get('/save/:id', homeController.saveUser)

web.get('/delete', homeController.deleteUser)


module.exports = web