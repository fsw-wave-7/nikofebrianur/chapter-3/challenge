const express = require('express') /// impor express
const bodyParser = require('body-parser') /// impor bodyParser
const jsonParser = bodyParser.json() /// declare jsonParser

const User = require('../controllers/user.js') /// impor class User
const user = new User /// declare user

/// function middleware logger
const logger = (req,res,next) => {
    console.log('LOG: ${req.method} ${req.url}')
    next()
}

/// declare api
const api = express.Router()

api.use(logger) /// function logger
api.use(jsonParser) /// function jsonParser

/// endpoint
api.get('/user/', user.getUser) /// get
api.get('/user/:index', user.getDetailUser) /// get
api.post('/user/', user.addUser) /// post
api.put('/user/:index', jsonParser, user.updateUser) /// put
api.delete('/user/:index', jsonParser, user.deleteUser) /// delete

module.exports = api /// export route

