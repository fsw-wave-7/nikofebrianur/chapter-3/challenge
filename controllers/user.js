const {response} = require('../helpers/response.js') /// impor helper
const fs = require('fs') /// impor fs 
const readUser = fs.readFileSync('./data/user.json') /// declare fs

/// class dan method User
class User {
    constructor() {
        this.user = []
    }

    getUser = (req,res) => {
        this.user = JSON.parse(readUser)
        response(
            res,200,this.user,{total:readUser.length}
        )
    }

    getDetailUser = (req,res) => {
        const index = req.params.index
        this.user = JSON.parse(readUser) 

        response(res,200,this.user[index])
    }

    addUser = (req,res) => {
        this.user = JSON.parse(readUser) 
        const body = req.body

        const param = {
            "name": body.name,
            "username": body.username,
            "password": body.password,
            "email": body.email
        }

        this.user.push(param)
        fs.writeFileSync('./data/user.json', JSON.stringify(this.user))
        response(res,200,param)
    }

    updateUser = (req,res) => {
        const index = req.params.index
        const body = req.body
        
        this.user[index].name = body.name
        this.user[index].username = body.username
        this.user[index].password = body.passsword
        this.user[index].email = body.email

        fs.writeFileSync('./data/user.json', JSON.stringify(this.user))
        response(res,200,this.user[index])
    }

    deleteUser = (req,res) => {
        this.user = JSON.parse(readUser)
        const index = req.params.index
        
        this.user.splice(index, 1)

        fs.writeFileSync('./data/user.json',
        JSON.stringify(this.user))
        response(res,200,null)
    }
}

module.exports = User /// export class