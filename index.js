const express = require('express') /// impor express
const morgan = require('morgan') /// impor morgan
const path = require('path')
const { join } = require('path')
const api = require('./routes/api.js') //// impor routes
const web = require('./routes/web.js')

const app = express() /// load express
const port = 3000 /// port

/// load view engine 
app.set('view engine', 'ejs')

/// load static files
app.use(express.static(__dirname + '/public'))

/// load morgan
app.use(morgan('dev'))

/// load routes
app.use('/api', api)
web.use('/, web')

// internal server
app.use(function(err, req, res, next) {
    res.status(500).json({
        status: 'fail',
        errors: err.message
    })
})

/// 404 handling error
app.use(function(req,res,next){
    res.render(join(__dirname, './views/404'))
})

/// cek server
app.listen(port, () => {console.log('Server ON cuy!') })